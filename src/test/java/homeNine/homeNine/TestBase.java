package homeNine.homeNine;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import homeNine.homeNine.GmailPageFactory;
import homeNine.homeNine.WebDriverFactory;

public class TestBase {

	private WebDriver webDriver;
	
	protected LoginPage loginPage;

	@BeforeMethod
	@Parameters({ "browserName" })
	
	//Все працює, крім параметрів. Чомусь не читає з файла браузер,а запускає той який прописаний в @Optional.
	
	public void setup(@Optional("firefox") String browserName) {
		
		webDriver = WebDriverFactory.getInstance(browserName);
		webDriver.get("https://www.gmail.com");
		loginPage = GmailPageFactory.initElements(webDriver, LoginPage.class);
	}

	@AfterMethod
	public void tearDown() throws Exception 	{
		if (webDriver != null) {
			WebDriverFactory.killDriverInstance();
		}
	}

}