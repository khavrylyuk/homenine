package homeNine.homeNine;

import org.testng.Assert;
//import org.testng.Assert;
import org.testng.annotations.Test;

import homeNine.homeNine.TestBase;

public class InboxTestSuite extends TestBase {

	@Test
	public void CheckEmailPresent() {
	    loginPage.enterLoginEmail("kos4test@gmail.com");
	    loginPage.clickEmailNextButton();
	    loginPage.enterPasswordEmail("mycodeworks9!");
	    InboxPage inboxPage = loginPage.clickPasswordNextButton();
	    inboxPage.enterSearchSubject("Hello User!");    
	    Assert.assertTrue(inboxPage.emailIsFound(), "Email 'Hello User!' is not present.");
	}
	
	@Test
	public void CheckEmailNotPresent() {
	    loginPage.enterLoginEmail("kos4test@gmail.com");
	    loginPage.clickEmailNextButton();
	    loginPage.enterPasswordEmail("mycodeworks9!");
	    InboxPage inboxPage = loginPage.clickPasswordNextButton();
	    inboxPage.enterSearchSubject("Bye User!");    
	    Assert.assertTrue(inboxPage.noEmailsFound(), "At least one email is found.");
	}
}
