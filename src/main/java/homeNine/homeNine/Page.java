package homeNine.homeNine;

import java.util.NoSuchElementException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class Page implements IPageLoaded {
	protected WebDriver webDriver;
	
	public Page (WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public WebDriver getWebDriver() {
		return webDriver;
	}
	
	public String getTitle() {
		return webDriver.getTitle();
	}
	
	public String getPageUrl() {
		return webDriver.getCurrentUrl();
	}
	
	public boolean isElementPresent (WebElement element) {
		try {
			element.isEnabled();
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	
	public void clickElement (WebElement element) {
		element.click();
	}
	
}
