package homeNine.homeNine;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.google.common.base.Function;

public class HomePage extends Page {
	
	@FindBy (xpath = "//a[@data-g-label=\"Sign in\"]")
	private WebElement loginButton;
	
	public void clickLoginButton() {
		loginButton.click();
	}
	
	
	
	public HomePage(WebDriver webDriver) {
		super(webDriver);
	}
	
	public Function<WebDriver, ?> isPageLoaded() {
		return ExpectedConditions.visibilityOf(loginButton);
	}
	
	

}
