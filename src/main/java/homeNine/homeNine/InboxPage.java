package homeNine.homeNine;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.google.common.base.Function;

public class InboxPage extends Page {
	
	public InboxPage(WebDriver webDriver) {
		super(webDriver);
		}

	@FindBy (xpath = "//input[@class='gb_Cf']")
	private WebElement SearchField;
	
	@FindBy (xpath = "//tr[@class='TD']")
	private WebElement EmptyEmailsList;
	
	@FindBy (xpath = "//div[@class='ae4 UI']/div/div/table/tbody/tr[1]")
	private WebElement EmailIsFound;
	
	public void enterSearchSubject(String subject) {
		SearchField.click();
		SearchField.sendKeys(subject);
		SearchField.sendKeys(Keys.ENTER);
	}
	
	public boolean noEmailsFound() {
		return EmptyEmailsList.isDisplayed();
	}
	
	public boolean emailIsFound() {
		return EmailIsFound.isDisplayed();
	}
	
	public Function<WebDriver, ?> isPageLoaded() {
		return ExpectedConditions.visibilityOf(SearchField);
	}

}
