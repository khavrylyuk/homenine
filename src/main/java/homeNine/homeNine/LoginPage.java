package homeNine.homeNine;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class LoginPage extends Page {
	
	@FindBy (xpath = "//input[@id=\"identifierId\"]")
	private WebElement enterEmailField;

	@FindBy (xpath= "//div[@id=\"identifierNext\"]")
	private WebElement emailNextButton;
	
	@FindBy (xpath= "//div[@id=\"passwordNext\"]")
	private WebElement passwordNextButton;
		
	@FindBy (xpath = "//input[@type=\"password\"]")
	private WebElement enterPasswordField;
	
	public InboxPage clickPasswordNextButton() {
		passwordNextButton.click();
		return GmailPageFactory.initElements(webDriver, InboxPage.class);
	}
	
	public void clickEmailNextButton() {
		emailNextButton.click();
	}
	
	public void enterLoginEmail(String email) {
		enterEmailField.click();
		enterEmailField.sendKeys(email);
	}
	
	public void enterPasswordEmail(String password) {
		WebDriverWait wait = new WebDriverWait(webDriver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(enterPasswordField));
		enterPasswordField.click();
		enterPasswordField.sendKeys(password);
	}
	
	public LoginPage(WebDriver webDriver) {
		super(webDriver);
	}

	public Function<WebDriver, ?> isPageLoaded() {
		return ExpectedConditions.visibilityOf(enterEmailField);
	}
}
