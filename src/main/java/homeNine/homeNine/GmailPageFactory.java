package homeNine.homeNine;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailPageFactory {
	
	@SuppressWarnings("unchecked")
	public static <T extends Page> T initElements(WebDriver webDriver, Class<T> clazz) {
		WebDriverWait wait = new WebDriverWait(webDriver, 30);
		Page page = PageFactory.initElements(webDriver, clazz);
		wait.until(page.isPageLoaded());
		
		return (T) page;
	}
	public static void initElements(WebDriver webDriver, Page page){
		WebDriverWait wait = new WebDriverWait(webDriver, 30);
		PageFactory.initElements(webDriver, page);
		wait.until(page.isPageLoaded());
	}

}