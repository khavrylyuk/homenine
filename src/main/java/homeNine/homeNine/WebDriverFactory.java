package homeNine.homeNine;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebDriverFactory {

	private static WebDriver webDriver;

	private WebDriverFactory() {

	}

	public static WebDriver getInstance(String browserName) {
//		if (webDriver == null) {
			if (browserName.equals("chrome")) {
				
				WebDriverManager.chromedriver().setup();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("test-type");
				options.addArguments("disable-infobars");
				webDriver = new ChromeDriver();
		
				
			} else if (browserName.equals("firefox")) {
				WebDriverManager.firefoxdriver().setup();
				webDriver = new FirefoxDriver();
				
			} else
				throw new IllegalArgumentException("Invalid browser property set in configuration file");
			
			webDriver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
			webDriver.manage().window().maximize();
			return webDriver;	
	}

		
//	}


	public static void killDriverInstance() {
		if (webDriver != null) {
			webDriver.quit();
			webDriver = null;
		}
	}

}